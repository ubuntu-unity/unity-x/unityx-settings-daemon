/* main.vala
 *
 * Licensed under GNU General Public License v3.0.
 */

using GLib;
using Posix;

void println (string str) {
    print (str + "\n");
}

int main (string[] args) {
    // Load unityx-settings-daemon settings
    var settings = new Settings ("org.unityx.settings-daemon");

    // Set GTK theme
    println ("Setting GTK theme.");
    GLib.Environment.set_variable ("GTK_THEME", settings.get_string ("gtk-theme"), true);

    // Load window manager
    println ("Loading window manager.");
    system (settings.get_string ("wm") + " &");

    // Load background
    println ("Loading background.");
    system ("feh --bg-fill " + settings.get_string ("background") + " &");

    // Load desktop icons
    println ("Loading desktop icons.");
    system ("nemo-desktop &");

    // Load panel
    println ("Loading panel.");
    system (settings.get_string ("panel") + " &");

    // Successful return code
    return 0;
}
