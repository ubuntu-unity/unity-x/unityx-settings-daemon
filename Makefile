# Makefile
#
# Licensed under GNU General Public License v3.0.

FILES=src/main.vala
ARGS=--pkg posix --pkg gio-2.0
VALAC=valac
OUTFILE=unityx-settings-daemon

out/$(OUTFILE): clean
	@echo 'Compiling...'
	mkdir out
	valac $(ARGS) $(FILES) --output='out/$(OUTFILE)'

.PHONY: test
test: out/$(OUTFILE)
	@echo ''
	@echo 'Running tests...'
	@out/unityx-settings-daemon &>/dev/null && { echo "Failed test: No arguments" && exit 1 ;} || echo "Successful test: No arguments."

.PHONY: install
install:
	@{ test -z "$$DESTDIR" && DESTDIR="" || true ;} \
	&& ( mkdir -p $$DESTDIR/usr $$DESTDIR/usr/share $$DESTDIR/usr/share/glib-2.0 \
	$$DESTDIR/usr/share/glib-2.0/schemas $$DESTDIR/usr/bin/ \
	&& cp gschemas/* $$DESTDIR/usr/share/glib-2.0/schemas \
	&& cp out/unityx-settings-daemon $$DESTDIR/usr/bin/unityx-settings-daemon \
	&& echo "To complete the installation (for actual use and not packaging), please run: glib-compile-schemas $$DESTDIR/usr/share/glib-2.0/schemas/" )
	@echo "Completed task."

.PHONY: clean
clean:
	@rm -rf out
